// reinventing the wheel: std::string
// based on std::string
// or even worse.

#include <cstdio>
#include <cstdlib>
#include <exception>
#include <stdexcept>
#include <iostream>

#ifndef FLAMINGOSTRING_H_
#define FLAMINGOSTRING_H_

namespace flamingo
{
    typedef size_t size_y;
    class string
    {
        #define X throw
        #ifndef E_ORANGE
            #define E_ORANGE std::out_of_range("Index out of bonds.")
        #endif    
    private:                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               // #define _ALLOCSTR(n) (char*)calloc(n+1,sizeof(char))
        char* str;
        size_y cap;

        /** allocate the array of char with size of n 
         * and also adds 1 more byte for the 
         * null character. */
        char* _allocstr(size_y n);

        /** counts the length of the string
         * not including the null character. */
        size_y _strlen(const char* s) const noexcept;

        /** compares strings
         * @return true if both strings are equal
         */
        bool _streq(const char* s1, const char* s2) const noexcept;

    public:

        /** an illogical constructor
         * or else I don't know what this does
         * in std::string */
        string();

        // a normal constructor
        string(const char *s);

        // a normal constructor, but different.
        string(const char *s, size_y n);

        // fill string with "useful" char
        string (size_y n, char c);

        // copy constructors of flamingo::string
        string (const flamingo::string& str);

        // copy constructors of flamingo::string with n.
        string (const flamingo::string& str, size_y n);

        // assignment operator
        string& operator= (const flamingo::string& str);
        string& operator= (const std::string& str);
        string& operator= (const char* s);
        string& operator= (char c);

        /* d-don't touch this.
         * usage example:
         * flamingo::string str;
         * str.operator$();
        */
        void operator$() const;
        
        // I have no idea for initializer_list
        // so does move constructor

        size_y size() const noexcept;
        size_y length() const noexcept;
        // dunno how to implement max_size() :(
        void resize(size_y n);
        void resize(size_y n, char c);
        // size_y capacity() const noexcept;
        void reserve(size_y n = 0);
        void clear() noexcept;
        bool empty() const noexcept;

        
        // element access

        char& operator[] (size_y pos);
        const char& operator[] (size_y pos) const;
        
        char& at(size_y pos);
        const char& at(size_y pos) const;

        char& back();
        const char& back() const;

        char& front();
        const char& front() const;

        // modifiers

        string& operator+= (const flamingo::string& str);
        string& operator+= (const std::string& str);
        string& operator+= (const char* s);
        string& operator+= (char c);

        const char* data() const;

        friend std::ostream& operator<<(std::ostream& os, flamingo::string& str);

        ~string();
    };
};

#endif