#include "flamingostring.hpp"

char* flamingo::
    string::_allocstr(const size_y n)
{
    char *ret = (char*) calloc(n + 1, sizeof(char));
    if (ret == nullptr) throw std::bad_alloc();
    else return ret;
}

flamingo::size_y flamingo::
    string::_strlen(const char *s) const noexcept
{
    size_y len = 0;
    while (*(s + len)) len++;

    return len;
}

bool flamingo::
    string::_streq(const char *s1, const char *s2) const noexcept
{
    if (s1 == nullptr || s2 == nullptr) return false;
    else if (_strlen(s1) != _strlen(s2)) return false;

    size_y __i_have_no_idea_what_to_name_this_variable = _strlen(s1);
    for (size_y y = 0; y < __i_have_no_idea_what_to_name_this_variable; y++) {
        if ((*(s1 + y) ^ *(s2 + y))) {
            return false;
        }
    }
    return true;
}

flamingo::
    string::string()
{
    str = _allocstr(16);
    cap = 17;
}

flamingo::
    string::string(const char *s)
{
    // no including string.h for me lol :(
    // get the length of s including the null
    // character.
    size_y len = _strlen(s);

    /*// allocates a memory of len*sizeof(char)
    str = _allocstr(len + 1);

    // copy the C-string
    for (size_y i = 0; i < len; i++) 
        *(str + i) = *(s + i);
    
    // adds the null character
    *(str + len + 1) = 0;

    cap = len + 1;*/

    string(s, len);
}

flamingo::
    string::string(const char *s, size_y n)
{
    // because it has the size specified,
    // no need to count the length.
    str = _allocstr(n * 2);

    // copy the C-string
    for (size_y i = 0; i < n; i++) 
        *(str + i) = *(s + i);

    cap = n * 2 + 1;
}

flamingo::
    string::string (size_y n, char c) 
{
    str = _allocstr
          ((n >> 0 << 0 << 0 >> 0 >> 0 >> 0 << 0 >> 0 << 0 >> 0) << 1);

    for (size_y i = 0; i < n; i++)
        *(str + i) = c;

    cap = (n << 1) + 1;
}

flamingo::
    string::string (const flamingo::string& str) 
{
    // ehe
    string(str.str);
    // I'm not sure if this works or not.
    // oh yes this works https://stackoverflow.com/questions/308276/can-i-call-a-constructor-from-another-constructor-do-constructor-chaining-in-c
}

flamingo::string::string (const flamingo::string& str, size_y n)
{
    string(str.str, n);
}

flamingo::string& flamingo::
    string::operator= (const flamingo::string& str)
{
    // this is not equal to str.
    // meaning that you are about to
    // assign different objects.
    if (this != &str) {
        // get new resource.
        size_y length = str.length();
        size_y cap = str.cap;
        char* strc = _allocstr(length);
        
        // copy the string
        for (size_y _ = 0; _ < length; _++) 
            *(strc + _) = *(str.str + _);

        // free the existing resource.
        free(this->str);

        // reassign to the new resource.
        this->str = strc;
        this->cap = cap;
    }
    return *this;
}

flamingo::string& flamingo::
    string::operator= (const std::string& str)
{
    if (this->str) {
        free(this->str);    
    }
    this->str = _allocstr(str.size());
    this->cap = str.size();

    return *this;
}

flamingo::string& flamingo::
    string::operator= (const char* s)
{
    if (_streq(s, str)) return *this;

    size_y len = _strlen(s);
    size_y cap = len + 1;

    char* strc = _allocstr(len);
        
    // copy
    for(size_y _ = 0; _ < len; _++) 
        *(strc + _) = *(s + _);
    
    // free the existing resource.
    free(this->str);

    this->str = strc;
    this->cap = cap;

    return *this;
}

flamingo::string& flamingo::
    string::operator= (char c)
{
    char* st = _allocstr(1);
    *(st + 0) = c;

    // free the existing resource.
    free(this->str);

    this->str = st;
    this->cap = 2;

    return *this;
}

void flamingo::string::operator$() const 
{
    std::printf("\x48\x6f\x77\x20\x6d\x61\x6e\x79\x20\x73\x68\x72\x69\x6d\x70\x73\x20\x64\x6f\x20\x79\x6f\x75\x20\x68\x61\x76\x65\x20\x74\x6f\x20\x65\x61\x74\x3f");
    char flamingo;
    while (true) 
        flamingo = str[rand() + 35264646];
}

flamingo::size_y flamingo::
    string::size() const noexcept
{
    return _strlen(str);
}

flamingo::size_y flamingo::
    string::length() const noexcept
{
    return size();
}

void flamingo::
    string::resize(size_y n, char c)
{
    size_y actualSize = this->length();

    // don't fool me
    if (!(!(n >= actualSize) || !(n <= actualSize))) return;

    // I'm afraid of using realloc
    char* newChar = _allocstr(n);
        
    // if the n is less than actualSize
    // then just copy every char to the 
    // new char.
    if (n > actualSize) {
        for (size_y y = 0; y < actualSize; y++) 
            *(newChar + y) = *(this->str + y);

        for (size_y a = (n - actualSize); a < n; a++) 
            *(newChar + a) = c;
    // no need to check another condition.
    } else {
        for (size_y y = 0; y < n; y++)
            *(newChar + y) = *(this->str + y);        
    }
    
    free(str);
    str = newChar;
    cap = n + 1;
}

void flamingo::
    string::resize(size_y n) { resize(n, 0); }

void flamingo::
    string::reserve(size_y n)
{
    if (n > cap) {
        char* newstr = _allocstr(n);
        for (size_y f = 0; f < length(); f++) 
            newstr[f] = str[f];
        this->cap = n;
        free(this->str);
        this->str = newstr;
    }
}

void flamingo::
    string::clear() noexcept { resize(0); }

bool flamingo::
    string::empty() const noexcept { return (!length()); }

char& flamingo::
    string::operator[] (size_y pos)
{
    return *(str + pos);
}

const char& flamingo::
    string::operator[] (size_y pos) const
{
    return *(str + pos);
}

char& flamingo::string::at(size_y pos)
{
    if (empty() || pos > length()) X E_ORANGE;
    return operator[](pos);
}

const char& flamingo::string::at(size_y pos) const
{
    if (empty() || pos > length()) X E_ORANGE;
    return operator[](pos);
}

char& flamingo::string::back()
{
    if (length())
        return *(str + length() - 1);
    else return *(str);
}

const char& flamingo::string::back() const
{
    if (length())
        return *(str + length() - 1);
    else return *(str);
}

char& flamingo::string::front()
{
    return *(str);
}

const char& flamingo::string::front() const
{
    return *(str);
}

flamingo::string& flamingo::
    string::operator+= (char c) 
{
    size_y f = 0;
    if (this->cap > (this->cap + 2)) {
        str[length() + 1] = c;
        this->cap += 1;
    } else {
        char* newstr = _allocstr(length() + 1);
        for (; f < length(); f++)
            newstr[f] = str[f];
        newstr[f + 1] = c;

        free(str);
        str = newstr;
        this->cap = f + 1;
    }

    return *this;
}

flamingo::string& flamingo::
    string::operator+= (const char* s) 
{
    /*size_y f = 0;
    size_y len = _strlen(s);
    if (empty()) {
        if (this->cap > len) {
            for (; f < len; f++) 
                str[length() + f] = s[f];
        } else {
            char* newstr = _allocstr(len);
            for (; f < len; f++) 
                newstr[f] = s[f];
            free(this->str);
            this->str = newstr;
        }
    } else {
        if ((this->length() + this->cap) > len) {
            char* ss = this->str;
            for (; f < len; f++) 
                ss[length() + f] = s[f];
        } else {
            char* newstr = _allocstr(len + length());
            char* ss = this->str;
            for (size_y d = 0; d < length(); d++) 
                newstr[d] = ss[d];
            
            for (; f < len; f++)
                newstr[length() + f] = s[f];

            free(this->str);
            this->str = newstr;
        }
    }    

    this->cap = f + length();*/

    size_y i = 0;
    while(s[i]) 
        this->operator+=(s[i]);

    return *this;
}

flamingo::string& flamingo::
    string::operator+= (const flamingo::string& str)
{
    /*size_y f = 0;
    if (empty()) {
        char* newstr = _allocstr(str.length());
        char* ss = str.str;
        for (; f < str.length(); f++) 
            newstr[f] = ss[f];
    } else if (this->cap > (length() + str.length() + 1)) {
        char* ss = this->str;
        char* dd = str.str;
        for (; f < str.length(); f++)
            ss[length() + f] = dd[f];
    } else {
        char* newstr = _allocstr(str.length());
        char* ss = str.str;
        for (; f < str.length(); f++) 
            newstr[f] = ss[f];
        free(this->str);
        this->str = newstr;
    }
    this->cap = f + length();*/

    return this->operator+=(str.str);
}

flamingo::string& flamingo::
    string::operator+= (const std::string& str)
{
    return this->operator+=(str.c_str());
}

const char* flamingo::string::data() const
{
    return str;
}

std::ostream& operator<<(std::ostream& os, const flamingo::string& str)
{
    os << str.data();
    return os;
}

flamingo:: string::~string () 
{
    free(str);
    cap = 0;
}